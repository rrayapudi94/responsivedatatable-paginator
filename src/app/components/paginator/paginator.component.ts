import { Component, Input, OnChanges, SimpleChange, Optional} from '@angular/core';
import { ResponsiveTableDirective, PageEvent } from '../../responsive-table.directive';


@Component({
  selector: 'app-paginator',
  template: `<ng-content></ng-content>`,
})
export class PaginatorComponent implements OnChanges {
  @Input("resTable") inputResTable: ResponsiveTableDirective;
  private mfTable: ResponsiveTableDirective;
  activePage: number;
  rowsOnPage: number;
  dataLength: number = 0;
  lastPage: number;

  constructor(@Optional() private injectResTable: ResponsiveTableDirective) {}

  ngOnChanges(changes: {[key: string]: SimpleChange}): any {
      this.mfTable = this.inputResTable || this.injectResTable;
      this.onPageChangeSubscriber(this.mfTable.getPage());
      this.mfTable.onPageChange.subscribe(this.onPageChangeSubscriber);
  }

  setPage(pageNumber: number): void {
      this.mfTable.setPage(pageNumber, this.rowsOnPage);
  }

  setRowsOnPage(rowsOnPage: any): void {
    this.rowsOnPage = parseInt(rowsOnPage);
    const rowsPerPage = this.rowsOnPage === null ? null : this.rowsOnPage;
      this.mfTable.setPage(this.activePage, this.rowsOnPage);
  }

  private onPageChangeSubscriber = (event: PageEvent)=> {
      this.activePage = event.activePage;
      this.rowsOnPage = event.rowsOnPage;
      this.dataLength = event.dataLength;
      this.lastPage = Math.ceil(this.dataLength / this.rowsOnPage);
  };

}
