import { Component, Input, OnChanges } from '@angular/core';
import { ResponsiveTableDirective, PageEvent } from '../../responsive-table.directive';
import {min} from 'lodash';

@Component({
  selector: 'app-res-paginator',
  templateUrl: './res-paginator.component.html',
  styleUrls: ['./res-paginator.component.scss']
})
export class ResPaginatorComponent implements OnChanges {

  @Input("rowsOnPageSet") rowsOnPageSet = [];
  @Input("resTable") mfTable: ResponsiveTableDirective;
  @Input("selctedRows") rowsPerPage: number;

  minRowsOnPage = 0;

  ngOnChanges(changes: any): any {
      if (changes.rowsOnPageSet) {
          this.minRowsOnPage = min(this.rowsOnPageSet)
      }
  }
}
