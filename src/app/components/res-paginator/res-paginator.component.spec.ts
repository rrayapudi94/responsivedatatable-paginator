import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResPaginatorComponent } from './res-paginator.component';

describe('ResPaginatorComponent', () => {
  let component: ResPaginatorComponent;
  let fixture: ComponentFixture<ResPaginatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResPaginatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResPaginatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
