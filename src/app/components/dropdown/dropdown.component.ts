import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent  {
  @Input() selectedValue:number;
  @Input() dropDownListItems: Array<any> = [];
  @Output() selectedItem = new EventEmitter();

  onSelectionChanged() {
    this.selectedItem.emit(this.selectedValue);
  }

}
