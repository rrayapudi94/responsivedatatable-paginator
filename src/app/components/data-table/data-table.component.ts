import {Component, Input, OnInit, Output} from '@angular/core';
import {AppService} from '../../app-service';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent {
  
  @Input() data: Array<any> = [];
  @Input() dataHeaders: Array<String> = [];
  @Input() showPagination = false;
  @Input() rowsPerPage: number = null;
  dropDrowRowsItems = [5, 10, 15];

  constructor(private apSrv: AppService) { }

  postRowDetails(item) {
    this.apSrv.postTableRow(item).subscribe(res=>{
      console.log(res);
    }, error => {
      console.log(error);
    });
  }
}
