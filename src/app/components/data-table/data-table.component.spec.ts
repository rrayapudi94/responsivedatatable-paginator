import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { DataTableComponent } from './data-table.component';
import { AppService } from '../../app-service';
import { of } from 'rxjs';

describe('DataTableComponent', () => {
  let component: DataTableComponent;
  let fixture: ComponentFixture<DataTableComponent>;

  beforeEach(async(() => {
    const message = {msg: 'succesfull'};
    const appService = jasmine.createSpyObj('AppService', ['postTableRow']);
  // Make the spy return a synchronous Observable with the test data
    const appServiceSpy = appService.postTableRow.and.returnValue( of(message));

    TestBed.configureTestingModule({
      declarations: [ DataTableComponent ],
      providers: [
        { provide: AppService, useClass: FakeAppService }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should post the item and succesfully retrieve deail', fakeAsync(() => {
    const itemSelected = {
      id: 9596999,
      name: 'oooo'
    };

    const service = TestBed.get(AppService)
    component.postRowDetails(itemSelected);
    tick();
    expect(service.postTableRow.toHaveBeenCalled);
  }));
});

export class FakeAppService {
  postTableRow() {
    return of({msg: 'succesfull'});
  }
}

