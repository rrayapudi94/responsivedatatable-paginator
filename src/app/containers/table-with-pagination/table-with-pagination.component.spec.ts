import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AppService} from '../../app-service';
import { TableWithPaginationComponent } from './table-with-pagination.component';
import { of } from 'rxjs';

describe('TableWithPaginationComponent', () => {
  let component: TableWithPaginationComponent;
  let fixture: ComponentFixture<TableWithPaginationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableWithPaginationComponent ],
      providers: [
        { provide: AppService, useClass: FakeAppService }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableWithPaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return the data on onit and set headers', () => {
    component.ngOnInit();
    fixture.detectChanges();
  
    expect(component.data).toEqual([{
      id: 9999,
      name: 'ppp'
    }]);
    expect(component.dataHeaders).toEqual(['id', 'name']);
  });
});

export class FakeAppService {
  getTableData() {
    const data = [{
      id: 9999,
      name: 'ppp'
    }];
    return of(data);
  }
}
