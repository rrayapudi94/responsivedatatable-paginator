import { Component, OnInit, OnDestroy } from '@angular/core';
import {AppService} from '../../app-service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-table-with-pagination',
  templateUrl: './table-with-pagination.component.html',
  styleUrls: ['./table-with-pagination.component.scss']
})
export class TableWithPaginationComponent implements OnInit, OnDestroy {
  data:Array<any> = [];
  dataHeaders: Array<String> =[];
  subscriptions = new Subscription();

  constructor(private apSrv: AppService) { }

  ngOnInit(): void {
    this.subscriptions.add(this.apSrv.getTableData().subscribe((res:Array<any>) => {
      this.data = res;
      this.getHeaders();
    }, err => { 
      console.log(err);
    }));
  }

  getHeaders() {
    this.dataHeaders = [];
    for(const item in  this.data[0]){
      this.dataHeaders.push(item);
    }
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

}
