import { Component, OnInit, OnDestroy } from '@angular/core';
import {AppService} from '../../app-service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-simple-data-table',
  templateUrl: './simple-data-table.component.html',
  styleUrls: ['./simple-data-table.component.scss']
})
export class SimpleDataTableComponent implements OnInit, OnDestroy {

  data:Array<any> = [];
  dataHeaders: Array<String> =[];
  subscriptions = new Subscription();

  constructor(private apSrv: AppService) { }

  ngOnInit(): void {
    this.subscriptions.add(this.apSrv.getTableData().subscribe((res:Array<any>) => {
      this.data = res;
      this.getHeaders();
    }, err => { 
      console.log(err);
    }));
  }

  getHeaders() {
    this.dataHeaders = [];
    for(const item in  this.data[0]){
      this.dataHeaders.push(item);
    }
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
