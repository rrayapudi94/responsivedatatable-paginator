import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { SimpleDataTableComponent } from './simple-data-table.component';
import { AppService } from '../../app-service';

describe('SimpleDataTableComponent', () => {
  let component: SimpleDataTableComponent;
  let fixture: ComponentFixture<SimpleDataTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpleDataTableComponent ],
      providers: [
        { provide: AppService, useClass: FakeAppService }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleDataTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return the data on onit and set headers', () => {
    component.ngOnInit();
    fixture.detectChanges();
  
    expect(component.data).toEqual([{
      id: 9999,
      name: 'ppp'
    }]);
    expect(component.dataHeaders).toEqual(['id', 'name']);
  });
});

export class FakeAppService {
  getTableData() {
    const data = [{
      id: 9999,
      name: 'ppp'
    }];
    return of(data);
  }
}
