import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DataTableComponent } from './components/data-table/data-table.component';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { HttpClientModule } from '@angular/common/http';
import { ResponsiveTableDirective } from  './responsive-table.directive';
import { ResPaginatorComponent } from './components/res-paginator/res-paginator.component';
import { PaginatorComponent } from './components/paginator/paginator.component';
import { AppService} from './app-service';
import { TableWithPaginationComponent } from './containers/table-with-pagination/table-with-pagination.component';
import { SimpleDataTableComponent } from './containers/simple-data-table/simple-data-table.component';

@NgModule({
  declarations: [
    AppComponent,
    ResponsiveTableDirective,
    DataTableComponent,
    DropdownComponent,
    ResPaginatorComponent,
    PaginatorComponent,
    TableWithPaginationComponent,
    SimpleDataTableComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
