import { TestBed } from '@angular/core/testing';
import {of} from 'rxjs'
import { AppService } from './app-service';
import {HttpClientModule, HttpErrorResponse} from '@angular/common/http';

describe('AppService', () => {
  let appService: AppService;
  let httpClientSpy: { get: jasmine.Spy, post: jasmine.Spy };

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);
    appService = new AppService(<any> httpClientSpy);
  });

  it('should return table rows data', () => {
    const usersData = [
      {
      id: 9896887,
      name: 'Tom',
      user: 'Admin'
    },
    {
      id: 9896899,
      name: 'Tommmy',
      user: 'User'
    },
  ];

  httpClientSpy.get.and.returnValue(of(usersData));

  appService.getTableData().subscribe(
    users => expect(users).toEqual(usersData),
  );
  expect(httpClientSpy.get.calls.count()).toBe(1);
  });

  it('should return an error when the server returns a 404', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'test 404 error',
      status: 404, statusText: 'Not Found'
    });
  
    httpClientSpy.get.and.returnValue(of(errorResponse));
  
    appService.getTableData().subscribe(
      heroes => {},
      error  => expect(error.message).toContain('test 404 error')
    );
  });

  it('should succesfully post the row data', () => {
    const item = {
      id: 9896899,
      name: 'Tommmy',
      user: 'User'
    }
    const successMsg = {
     message: 'Posted Succesfullly'
    };

  httpClientSpy.post.and.returnValue(of(successMsg));

  appService.postTableRow(item).subscribe(
    res => expect(res).toEqual(successMsg),
  );

  expect(httpClientSpy.post.calls.count()).toBe(1);
  });
});



