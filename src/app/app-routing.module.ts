import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TableWithPaginationComponent } from './containers/table-with-pagination/table-with-pagination.component';
import { SimpleDataTableComponent } from './containers/simple-data-table/simple-data-table.component';

const routes: Routes = [
  { path: '',   redirectTo: '/simple-data-table', pathMatch: 'full' },
  { path: 'simple-data-table', component: SimpleDataTableComponent },
  { path: 'table-with-pagination', component: TableWithPaginationComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
