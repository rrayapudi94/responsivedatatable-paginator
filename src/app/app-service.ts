import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AppService {

  constructor(private http: HttpClient) { }

  getTableData(){
    return this.http.get('/assets/sample_data.json');
  }

  postTableRow(tableRow) {
   return this.http.post('/data/item', tableRow);
  }
}
