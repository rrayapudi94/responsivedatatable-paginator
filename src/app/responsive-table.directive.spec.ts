import {SimpleChange, Component} from "@angular/core";
import {ResponsiveTableDirective, PageEvent } from "./responsive-table.directive";
import {TestBed, ComponentFixture} from "@angular/core/testing";
import {By} from "@angular/platform-browser";
import {switchMap} from 'rxjs/operators';
import * as _ from "lodash";

@Component({
    template: `<table [resTableData]="[]"></table>`
})
export class TestComponent {}

describe("DataTable directive tests", ()=> {
    let datatable: ResponsiveTableDirective;
    let fixture: ComponentFixture<TestComponent>;

    beforeEach(()=> {
        TestBed.configureTestingModule({
            declarations: [ResponsiveTableDirective, TestComponent]
        });
        fixture = TestBed.createComponent(TestComponent);
        datatable = fixture.debugElement.query(By.directive(ResponsiveTableDirective)).injector.get(ResponsiveTableDirective) as ResponsiveTableDirective;

        datatable.inputData = [
            {id: 3, name: 'banana'},
            {id: 1, name: 'Duck'},
            {id: 2, name: 'ącki'},
            {id: 5, name: 'Ðrone'},
            {id: 4, name: 'Ananas'}
        ];
        datatable.ngOnChanges({inputData: new SimpleChange(null, datatable.inputData, false)});
    });

    describe("initializing", ()=> {

        it("data should be empty array if inputData is undefined or null", () => {
            datatable.ngOnChanges({inputData: new SimpleChange(null, null, false)});
            datatable.ngDoCheck();
            expect(datatable.data).toEqual([]);
        });

        it("data should be equal to inputData", ()=> {
            datatable.rowsOnPage = null;
            datatable.ngDoCheck();
            expect(datatable.data).toEqual(datatable.inputData);
        });

        it("data should be 2 first items", ()=> {
            datatable.rowsOnPage = 2;
            datatable.ngDoCheck();
            expect(datatable.data).toEqual([{id: 3, name: 'banana'}, {id: 1, name: 'Duck'}]);
        });

        it("data should be 3. and 4. items", ()=> {
            datatable.rowsOnPage = 2;
            datatable.activePage = 2;
            datatable.ngDoCheck();
            expect(datatable.data).toEqual([{id: 2, name: 'ącki'}, {id: 5, name: 'Ðrone'}]);
        });

        it("shouldn't recalculate data when no changes", ()=> {
            datatable.ngDoCheck();
            let data = datatable.data;
            datatable.ngOnChanges({});
            datatable.ngDoCheck();
            expect(datatable.data).toBe(data);
        });
    });

    describe("pagination", ()=> {

        beforeEach(()=> {
            datatable.rowsOnPage = 2;
            datatable.ngDoCheck();
        });

        it("should return current page settings", ()=> {
            expect(datatable.getPage()).toEqual({activePage: 1, rowsOnPage: 2, dataLength: 5});
        });

        it("data should be 3. and 4. items when page change", ()=> {
            datatable.setPage(2, 2);
            datatable.ngDoCheck();
            expect(datatable.data).toEqual([{id: 2, name: 'ącki'}, {id: 5, name: 'Ðrone'}]);
        });

        it("data should be three first items when page change", ()=> {
            datatable.setPage(1, 3);
            datatable.ngDoCheck();
            expect(datatable.data).toEqual([{id: 3, name: 'banana'}, {id: 1, name: 'Duck'}, {id: 2, name: 'ącki'}]);
        });

        it("data should be two last items when page change", ()=> {
            datatable.setPage(2, 3);
            datatable.setPage(2, 3);
            datatable.ngDoCheck();
            expect(datatable.data).toEqual([{id: 5, name: 'Ðrone'}, {id: 4, name: 'Ananas'}]);
        });

        it("should change rowsOnPage when mfRowsOnPage changed", (done)=> {
            datatable.rowsOnPage = 2;
            datatable.ngDoCheck();
            expect(datatable.data).toEqual([{id: 3, name: 'banana'}, {id: 1, name: 'Duck'}]);

            datatable.onPageChange.subscribe((pageOptions: PageEvent)=> {
                expect(pageOptions.rowsOnPage).toEqual(3);
                done();
            });

            datatable.rowsOnPage = 3;
            datatable.ngOnChanges({rowsOnPage: new SimpleChange(2, 3, false)});
            datatable.ngDoCheck();
            expect(datatable.data).toEqual([{id: 3, name: 'banana'}, {id: 1, name: 'Duck'}, {id: 2, name: 'ącki'}]);


        });
    });

    describe("data change", ()=> {

        it("should refresh data when inputData change", ()=> {
            datatable.rowsOnPage = 2;
            let newData = [{id: 5, name: 'Ðrone'}, {id: 4, name: 'Ananas'}];
            datatable.ngOnChanges({inputData: new SimpleChange(datatable.inputData, newData, false)});
            datatable.ngDoCheck();
            expect(datatable.data).toEqual([{id: 5, name: 'Ðrone'}, {id: 4, name: 'Ananas'}]);
        });

        it("should refresh data when rows removed from inputData", ()=> {
            datatable.rowsOnPage = null;
            datatable.ngDoCheck();
            expect(datatable.data).toEqual(datatable.inputData);
            datatable.inputData.pop();
            datatable.ngDoCheck();
            expect(datatable.data).toEqual(datatable.inputData);
        });

        it("should refresh data when rows added to inputData", ()=> {
            datatable.rowsOnPage = null;
            datatable.ngDoCheck();
            expect(datatable.data).toEqual(datatable.inputData);
            datatable.inputData.push({id: 6, name: 'Furby'});
            datatable.ngDoCheck();
            expect(datatable.data).toEqual(datatable.inputData);
        });

        it("should fire onPageChange event after inputData change", (done)=> {
            datatable.setPage(2, 2);
            datatable.ngDoCheck();

            datatable.onPageChange.subscribe((opt: PageEvent)=> {
                expect(opt.activePage).toEqual(1);
                expect(opt.dataLength).toEqual(2);
                expect(opt.rowsOnPage).toEqual(2);
                done();
            });
            let newData = [{id: 5, name: 'Ðrone'}, {id: 4, name: 'Ananas'}];
            datatable.ngOnChanges({inputData: new SimpleChange(datatable.inputData, newData, false)});
            datatable.ngDoCheck();
        });

        it("should fire onPageChange event after rows added", (done)=> {
            datatable.setPage(2, 2);
            datatable.ngDoCheck();

            datatable.onPageChange.subscribe((opt: PageEvent)=> {
                expect(opt.activePage).toEqual(2);
                expect(opt.dataLength).toEqual(6);
                expect(opt.rowsOnPage).toEqual(2);
                done();
            });
            datatable.inputData.push({id: 6, name: 'Furby'});
            datatable.ngDoCheck();
        });

        it("should fire onPageChange event after rows removed", (done)=> {
            datatable.setPage(2, 2);
            datatable.ngDoCheck();

            datatable.onPageChange.subscribe((opt: PageEvent)=> {
                expect(opt.activePage).toEqual(1);
                expect(opt.dataLength).toEqual(2);
                expect(opt.rowsOnPage).toEqual(2);
                done();
            });
            _.times(3, ()=>datatable.inputData.pop());
            datatable.ngDoCheck();
        });

        it("should change page when no data on current page after changed inputData", ()=> {
            datatable.setPage(2, 2);
            datatable.ngDoCheck();

            let newData = [{id: 5, name: 'Ðrone'}, {id: 4, name: 'Ananas'}];
            datatable.ngOnChanges({inputData: new SimpleChange(datatable.inputData, newData, false)});
            datatable.ngDoCheck();
            expect(datatable.data).toEqual(newData);
        });

        it("should change page when no data on current page after rows removed", ()=> {
            datatable.setPage(2, 2);
            datatable.ngDoCheck();
            expect(datatable.data).toEqual([{id: 2, name: 'ącki'}, {id: 5, name: 'Ðrone'}]);

            datatable.inputData.pop();
            datatable.inputData.pop();
            datatable.inputData.pop();
            datatable.ngDoCheck();
            expect(datatable.data).toEqual([{id: 3, name: 'banana'}, {id: 1, name: 'Duck'}]);
        });

        it("shouldn't change page when can display data after data changed", ()=> {
            datatable.setPage(2, 1);
            datatable.ngDoCheck();

            let newData = [{id: 5, name: 'Ðrone'}, {id: 1, name: 'Duck'}, {id: 4, name: 'Ananas'}];
            datatable.ngOnChanges({inputData: new SimpleChange(datatable.inputData, newData, false)});
            datatable.ngDoCheck();
            expect(datatable.data).toEqual([{id: 1, name: 'Duck'}]);
        });

        it("shouldn't change page when can display data after rows removed", ()=> {
            datatable.setPage(2, 1);
            datatable.ngDoCheck();
            expect(datatable.data).toEqual([{id: 1, name: 'Duck'}]);

            datatable.inputData.pop();
            datatable.ngDoCheck();
            expect(datatable.data).toEqual([{id: 1, name: 'Duck'}]);
        });

        it("shouldn't change page when can display data after rows added", ()=> {
            datatable.setPage(2, 1);
            datatable.ngDoCheck();
            expect(datatable.data).toEqual([{id: 1, name: 'Duck'}]);

            datatable.inputData.push({id: 6, name: 'Furby'});
            datatable.ngDoCheck();
            expect(datatable.data).toEqual([{id: 1, name: 'Duck'}]);
        });

        it("shouldn't change page to 0 when data is empty", ()=> {
            datatable.setPage(2, 1);
            datatable.ngDoCheck();

            let newData = [];
            datatable.ngOnChanges({inputData: new SimpleChange(datatable.inputData, newData, false)});
            datatable.ngDoCheck();
            expect(datatable.activePage).toEqual(1);
        });

        it("shouldn't change page to 0 when data is empty after removed rows", ()=> {
            datatable.setPage(2, 1);
            datatable.ngDoCheck();

            _.times(5, ()=>datatable.inputData.pop());
            datatable.ngDoCheck();
            expect(datatable.inputData.length).toEqual(0);
            expect(datatable.activePage).toEqual(1);
        });
    });
});