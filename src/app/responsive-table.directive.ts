import { OnChanges, DoCheck, Directive, IterableDiffer, Input ,EventEmitter, IterableDiffers, SimpleChange} from '@angular/core';

export interface PageEvent{
    activePage: number;
    rowsOnPage: number;
    dataLength: number;
}

@Directive({
    selector: 'table[resTableData]',
    exportAs: 'resTable'
})
export class ResponsiveTableDirective implements OnChanges, DoCheck {
    private diff: IterableDiffer<any>;
    @Input('resTableData') public inputData: any[] = [];
    @Input('resRowsOnPage') public rowsOnPage = 1;
    @Input('resActivePage') public activePage = 1;

    private mustReCalculateData = false;
    public data : any[];
    public onPageChange = new EventEmitter<PageEvent>();

    constructor(private differs: IterableDiffers) {
        this.diff = this.differs.find([]).create(null);
    }

     getPage(): PageEvent {
        return {activePage: this.activePage, rowsOnPage: this.rowsOnPage, dataLength: this.inputData.length};
    }

     setPage(activePage: number, rowsOnpage: number) {
        if(this.rowsOnPage !== rowsOnpage || this.activePage !== activePage) {
            this.activePage = this.activePage !== activePage ? activePage : this.calculateNewActivePage(this.rowsOnPage, rowsOnpage);
            this.rowsOnPage = rowsOnpage;
            this.mustReCalculateData = true;
            this.onPageChange.emit({
                activePage: this.activePage,
                rowsOnPage: this.rowsOnPage,
                dataLength: this.inputData ? this.inputData.length : 0
            });
        }
    }

    calculateNewActivePage(prevPageRows: number, currPageRows:number):number {
        let firstRowOnPage = Math.ceil((this.activePage -1) * prevPageRows + 1);
        let newActivePage = Math.ceil(firstRowOnPage / currPageRows);

        return newActivePage;
    }

    private recaluclatePage() {
        let lastPage = Math.ceil(this.inputData.length / this.rowsOnPage);
        this.activePage = lastPage < this.activePage ? lastPage : this.activePage;
        this.activePage = this.activePage || 1;

        this.onPageChange.emit({
            activePage: this.activePage,
            rowsOnPage: this.rowsOnPage,
            dataLength: this.inputData.length
        });

    }

    ngOnChanges(changes:{[key:string]:SimpleChange}) {
        const changesOnPage = changes['rowsOnPage']
        if (changesOnPage) {
            this.rowsOnPage = changesOnPage.previousValue;
            this.setPage(this.activePage, changesOnPage.currentValue);
            this.mustReCalculateData = true;
        }
        if (changes["inputData"]) {
            this.inputData = changes["inputData"].currentValue || [];
            this.recaluclatePage();
            this.mustReCalculateData = true;
        }
    }

    public ngDoCheck(): any {
        let changes = this.diff.diff(this.inputData);
        if (changes) {
            this.recaluclatePage();
            this.mustReCalculateData = true;
        }
        if (this.mustReCalculateData) {
            this.fillData();
            this.mustReCalculateData = false;
        }
    }

    private fillData(): void {
        this.activePage = this.activePage;
        this.rowsOnPage = this.rowsOnPage;

        let offset = (this.activePage - 1) * this.rowsOnPage;
        let data = this.inputData;
        data =  this.rowsOnPage === null? this.inputData : this.inputData.slice(offset, offset + this.rowsOnPage);
        this.data = data;
    }
    
}